package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Me extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
    }
    public void back(View view){
        Button web  = (Button)findViewById(R.id.button4);
        Intent intent = new Intent(Me.this,MainActivity.class);
        startActivity(intent);
    }
}